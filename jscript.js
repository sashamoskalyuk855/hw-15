// перетворення числа в рядок
function str() {
    let x = 12345
    return x.toString()
}
console.log(str())


// перевірка числа на парність
function even_or_odd(number) {
    return number % 2 === 0 ? "Odd" : "Even"
}
console.log(even_or_odd(22))
console.log(even_or_odd(21))


// перевірка обєкта на наявність полів
let user = {
    name: "Саша",
    age: 28,
    dream: "be a programmer"
}

function count(user) {
    return Object.keys(user).length
}
console.log(count(user))


